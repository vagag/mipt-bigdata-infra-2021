# Bigdata infrastructure course
* [Таблица результатов](https://docs.google.com/spreadsheets/d/1ohDdHefRp1fIyfoiDmCorqXxAziqSqDzVUDuSunbqdU/edit?usp=sharing)

| task-name   | deadline    | penalty |
|-------------|-------------|---------|
| http        | 01.03 23:59 |    0%   |
| ram-segment | 08.03 23:59 |    0%   |
| index-me    | 15.03 23:59 |    0%   |
| bm25        | 26.03 23:59 |    50%  |
| plain       | 31.03 23:59 |    50%  |
| compression | 31.03 23:59 |    50%  |